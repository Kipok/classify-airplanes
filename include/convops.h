#pragma once

#include "matrix.h"

class SobelVert // vertical sobel, works with GreyImage
{
public:
    const int vert_radius;
    const int hor_radius;
    SobelVert() : vert_radius(1), hor_radius(0) {}
    double operator () (const Image &mat) const
    {
        return -mat(0, 0) + mat(2, 0);
    }
};

class SobelHor
{
public:
    const int vert_radius;
    const int hor_radius;
    SobelHor() : vert_radius(0), hor_radius(1) {}
    double operator () (const Image &mat) const
    {
        return -mat(0, 0) + mat(0, 2); 
    }
};