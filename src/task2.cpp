#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <iterator>

#include "matrix.h"
#include "classifier.h"
#include "EasyBMP.h"
#include "linear.h"
#include "argvparser.h"
#include "convops.h"

using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;

using CommandLineProcessing::ArgvParser;

typedef vector<pair<BMP*, int> > TDataSet;
typedef vector<pair<string, int> > TFileList;
typedef vector<pair<vector<float>, int> > TFeatures;

// Load list of files and its labels from 'data_file' and
// stores it in 'file_list'
void LoadFileList(const string& data_file, TFileList* file_list) {
    ifstream stream(data_file.c_str());

    string filename;
    int label;
    
    int char_idx = data_file.size() - 1;
    for (; char_idx >= 0; --char_idx)
        if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
            break;
    string data_path = data_file.substr(0,char_idx+1);
    
    while(!stream.eof() && !stream.fail()) {
        stream >> filename >> label;
        if (filename.size())
            file_list->push_back(make_pair(data_path + filename, label));
    }

    stream.close();
}

// Load images by list of files 'file_list' and store them in 'data_set'
void LoadImages(const TFileList& file_list, TDataSet* data_set) {
    for (size_t img_idx = 0; img_idx < file_list.size(); ++img_idx) {
            // Create image
        BMP* image = new BMP();
            // Read image from file
        image->ReadFromFile(file_list[img_idx].first.c_str());
            // Add image and it's label to dataset
        data_set->push_back(make_pair(image, file_list[img_idx].second));
    }
}

// Save result of prediction to file
void SavePredictions(const TFileList& file_list,
                     const TLabels& labels, 
                     const string& prediction_file) {
        // Check that list of files and list of labels has equal size 
    assert(file_list.size() == labels.size());
        // Open 'prediction_file' for writing
    ofstream stream(prediction_file.c_str());

        // Write file names and labels to stream
    for (size_t image_idx = 0; image_idx < file_list.size(); ++image_idx)
        stream << file_list[image_idx].first << " " << labels[image_idx] << endl;
    stream.close();
}

enum MatMode {
    TO_GRAY,
    TO_R,
    TO_G,
    TO_B
};

Image BMP2Mat(BMP *bmp, MatMode mode=TO_GRAY)
{
    Image res(bmp->TellHeight(), bmp->TellWidth());
    for (uint i = 0; i < res.n_rows; ++i) {
        for (uint j = 0; j < res.n_cols; ++j) {
            RGBApixel *p = (*bmp)(j, i);
            if (mode == TO_GRAY) {
                res(i, j) = p->Red *0.299 + p->Green * 0.587 + p->Blue * 0.114;
            }
            if (mode == TO_R) {
                res(i, j) = p->Red;
            }
            if (mode == TO_G) {
                res(i, j) = p->Green;
            }
            if (mode == TO_B) {
                res(i, j) = p->Blue;
            }
        }
    }
    return res;
}

void SaveImage(const Image &im, const char *path)
{
    BMP out;
    out.SetSize(im.n_cols, im.n_rows);

    RGBApixel p;
    p.Alpha = 255;
    for (uint i = 0; i < im.n_rows; ++i) {
        for (uint j = 0; j < im.n_cols; ++j) {
            uint px = im(i, j);
            p.Red = px; p.Green = px; p.Blue = px;
            out.SetPixel(j, i, p);
        }
    }

    if (!out.WriteToFile(path)) {
        throw string("Error writing file ") + string(path);
    }
}

vector <float> ComputeCellHOG(const Image &chor, const Image &cvert, int num_segments)
{
    const double pi = 3.14159265;
    vector <float> res(num_segments);
    for (uint i = 0; i < res.size(); i++) {
        res[i] = 0;
    }
    for (uint y = 0; y < chor.n_rows; y++) {
        for (uint x = 0; x < chor.n_cols; x++) {
            double a = atan2(chor(y, x), cvert(y, x));
            int seg = (a + pi) * num_segments / (2 * pi);
            seg = seg == num_segments ? seg - 1 : seg;
            res[seg] += sqrt(chor(y, x) * chor(y, x) + cvert(y, x) * cvert(y, x));
        }
    }
    return res;
}

void normalize(vector<float>::iterator first, vector<float>::iterator last, vector<float>::iterator res)
{
    auto tmp = first;
    double sum = 0;
    while (tmp != last) {
        sum += *tmp * *tmp;
        ++tmp;
    }
    if (fabs(sum) < 1e-7) {
        return;
    }
    sum = sqrt(sum);
    while (first != last) {
        *res = *first / sum;
        ++first;
        ++res;
    }
}

vector <float> ComputeHOG(const Image &hor, const Image &vert, int num_segments, int num_hor_cells, int num_vert_cells)
{
    vector <float> res(num_hor_cells * num_vert_cells * num_segments);
    int h_step = hor.n_cols / num_hor_cells;
    int v_step = hor.n_rows / num_vert_cells;
    for (int v_cell = 0; v_cell < num_vert_cells; v_cell++) {
        for (int h_cell = 0; h_cell < num_hor_cells; h_cell++) {
            // if last cell -- take whole left image
            int h_size = h_cell == num_hor_cells - 1 ? hor.n_cols - h_cell * h_step : h_step;
            int v_size = v_cell == num_vert_cells - 1 ? hor.n_rows - v_cell * v_step : v_step;
            Image hcell_img = hor.submatrix(v_cell * v_step, h_cell * h_step, v_size, h_size);
            Image vcell_img = vert.submatrix(v_cell * v_step, h_cell * h_step, v_size, h_size);
            auto cell_res = ComputeCellHOG(hcell_img, vcell_img, num_segments);
            // maybe we should try normilize more than one cell
            normalize(cell_res.begin(), cell_res.end(), res.begin() + (v_cell * num_hor_cells + h_cell) * num_segments);
        }
    }
    return res;
}

vector <float> NonLinKernel(const vector <float> &features)
{
    const int n = 3;
    const double l = 1.02;
    const double pi = 3.14159265359;
    vector <float> res;
    for (uint i = 0; i < features.size(); i++) {
        for (int k = -n; k <= n; k++) {
            double lambda = k * l;
            double x = features[i];
            if (fabs(x) < 1e-7) {
                res.push_back(0.0);
                res.push_back(0.0);
                continue;
            }
            double f_im = - sqrt(x / cosh(pi * lambda)) * sin(lambda * log(x));
            double f_re = sqrt(x / cosh(pi * lambda)) * cos(lambda * log(x));
            res.push_back(f_im);
            res.push_back(f_re);
        }
    }
    return res;
}

float GetCellColor(const Image &cell)
{
    float avr = 0;
    for (uint y = 0; y < cell.n_rows; y++) {
        for (uint x = 0; x < cell.n_cols; x++) {
            avr += cell(y, x);
        }
    }
    return avr / (cell.n_rows * cell.n_cols * 255.0);
}

vector <float> GetColorFeatures(const Image &channel, int num_hor_cells, int num_vert_cells)
{
    vector <float> res;
    int h_step = channel.n_cols / num_hor_cells;
    int v_step = channel.n_rows / num_vert_cells;
    for (int v_cell = 0; v_cell < num_vert_cells; v_cell++) {
        for (int h_cell = 0; h_cell < num_hor_cells; h_cell++) {
            // if last cell -- take whole left image
            int h_size = h_cell == num_hor_cells - 1 ? channel.n_cols - h_cell * h_step : h_step;
            int v_size = v_cell == num_vert_cells - 1 ? channel.n_rows - v_cell * v_step : v_step;
            Image cell_img = channel.submatrix(v_cell * v_step, h_cell * h_step, v_size, h_size);
            float cell_res = GetCellColor(cell_img);
            res.push_back(cell_res);
        }
    }
    return res;
}

// ExAtract features from dataset.
void ExtractFeatures(const TDataSet& data_set, TFeatures* features) {
    for (size_t image_idx = 0; image_idx < data_set.size(); ++image_idx) {
        
        Image img_hor = BMP2Mat(data_set[image_idx].first);
        Image img_vert = BMP2Mat(data_set[image_idx].first);
        img_hor = img_hor.unary_map(SobelHor());
        img_vert = img_vert.unary_map(SobelVert());

        const int num_segments = 9;
        const int num_hor_cells = 8;
        const int num_vert_cells = 8;
        const int num_color_vert = 8;
        const int num_color_hor = 8;
        const bool use_piramid = true;
        const bool use_color = true;
        const bool use_nonlin = true;

        auto feas = ComputeHOG(img_hor, img_vert, num_segments, num_hor_cells, num_vert_cells);
        if (use_piramid) {
            auto hog1 = ComputeHOG(img_hor.submatrix(0, 0, img_hor.n_rows / 2, img_hor.n_cols / 2),
                                   img_vert.submatrix(0, 0, img_vert.n_rows / 2, img_vert.n_cols / 2),
                                   num_segments, num_hor_cells, num_vert_cells);

            auto hog2 = ComputeHOG(img_hor.submatrix(0, img_hor.n_cols / 2, img_hor.n_rows / 2, img_hor.n_cols / 2),
                                   img_vert.submatrix(0, img_vert.n_cols / 2, img_vert.n_rows / 2, img_vert.n_cols / 2),
                                   num_segments, num_hor_cells, num_vert_cells);

            auto hog3 = ComputeHOG(img_hor.submatrix(img_hor.n_rows / 2, 0, img_hor.n_rows / 2, img_hor.n_cols / 2),
                                   img_vert.submatrix(img_vert.n_rows / 2, 0, img_vert.n_rows / 2, img_vert.n_cols / 2),
                                   num_segments, num_hor_cells, num_vert_cells);

            auto hog4 = ComputeHOG(img_hor.submatrix(img_hor.n_rows / 2, img_hor.n_cols / 2, img_hor.n_rows / 2, img_hor.n_cols / 2),
                                   img_vert.submatrix(img_vert.n_rows / 2, img_vert.n_cols / 2, img_vert.n_rows / 2, img_vert.n_cols / 2),
                                   num_segments, num_hor_cells, num_vert_cells);

            feas.insert(feas.end(), hog1.begin(), hog1.end());
            feas.insert(feas.end(), hog2.begin(), hog2.end());
            feas.insert(feas.end(), hog3.begin(), hog3.end());
            feas.insert(feas.end(), hog4.begin(), hog4.end());                                    
        }

        if (use_nonlin) {
            feas = NonLinKernel(feas);
        }

        if (use_color) {
            auto color_r = GetColorFeatures(BMP2Mat(data_set[image_idx].first, TO_R), num_color_hor, num_color_vert);
            auto color_g = GetColorFeatures(BMP2Mat(data_set[image_idx].first, TO_G), num_color_hor, num_color_vert);
            auto color_b = GetColorFeatures(BMP2Mat(data_set[image_idx].first, TO_B), num_color_hor, num_color_vert);
            feas.insert(feas.end(), color_r.begin(), color_r.end());
            feas.insert(feas.end(), color_g.begin(), color_g.end());
            feas.insert(feas.end(), color_b.begin(), color_b.end());
        }

        features->push_back(make_pair(feas, data_set[image_idx].second));
    }
}

// Clear dataset structure
void ClearDataset(TDataSet* data_set) {
        // Delete all images from dataset
    for (size_t image_idx = 0; image_idx < data_set->size(); ++image_idx)
        delete (*data_set)[image_idx].first;
        // Clear dataset
    data_set->clear();
}

// Train SVM classifier using data from 'data_file' and save trained model
// to 'model_file'
void TrainClassifier(const string& data_file, const string& model_file) {
        // List of image file names and its labels
    TFileList file_list;
        // Structure of images and its labels
    TDataSet data_set;
        // Structure of features of images and its labels
    TFeatures features;
        // Model which would be trained
    TModel model;
        // Parameters of classifier
    TClassifierParams params;
    
        // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
        // Load images
    LoadImages(file_list, &data_set);
        // Extract features from images
    ExtractFeatures(data_set, &features);

        // PLACE YOUR CODE HERE
        // You can change parameters of classifier here
    params.C = 0.01;
    TClassifier classifier(params);
        // Train classifier
    classifier.Train(features, &model);
        // Save model to file
    model.Save(model_file);
        // Clear dataset structure
    ClearDataset(&data_set);
}

// Predict data from 'data_file' using model from 'model_file' and
// save predictions to 'prediction_file'
void PredictData(const string& data_file,
                 const string& model_file,
                 const string& prediction_file) {
        // List of image file names and its labels
    TFileList file_list;
        // Structure of images and its labels
    TDataSet data_set;
        // Structure of features of images and its labels
    TFeatures features;
        // List of image labels
    TLabels labels;

        // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
        // Load images
    LoadImages(file_list, &data_set);
        // Extract features from images
    ExtractFeatures(data_set, &features);

        // Classifier 
    TClassifier classifier = TClassifier(TClassifierParams());
        // Trained model
    TModel model;
        // Load model from file
    model.Load(model_file);
        // Predict images by its features using 'model' and store predictions
        // to 'labels'
    classifier.Predict(features, model, &labels);

        // Save predictions
    SavePredictions(file_list, labels, prediction_file);
        // Clear dataset structure
    ClearDataset(&data_set);
}

int main(int argc, char** argv) {
    // Command line options parser
    ArgvParser cmd;
        // Description of program
    cmd.setIntroductoryDescription("Machine graphics course, task 2. CMC MSU, 2014.");
        // Add help option
    cmd.setHelpOption("h", "help", "Print this help message");
        // Add other options
    cmd.defineOption("data_set", "File with dataset",
        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("model", "Path to file to save or load model",
        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("predicted_labels", "Path to file to save prediction results",
        ArgvParser::OptionRequiresValue);
    cmd.defineOption("train", "Train classifier");
    cmd.defineOption("predict", "Predict dataset");
        
        // Add options aliases
    cmd.defineOptionAlternative("data_set", "d");
    cmd.defineOptionAlternative("model", "m");
    cmd.defineOptionAlternative("predicted_labels", "l");
    cmd.defineOptionAlternative("train", "t");
    cmd.defineOptionAlternative("predict", "p");

        // Parse options
    int result = cmd.parse(argc, argv);

        // Check for errors or help option
    if (result) {
        cout << cmd.parseErrorDescription(result) << endl;
        return result;
    }

        // Get values 
    string data_file = cmd.optionValue("data_set");
    string model_file = cmd.optionValue("model");
    bool train = cmd.foundOption("train");
    bool predict = cmd.foundOption("predict");

        // If we need to train classifier
    if (train) {
        TrainClassifier(data_file, model_file);
    }
        // If we need to predict data
    if (predict) {
            // You must declare file to save images
        if (!cmd.foundOption("predicted_labels")) {
            cerr << "Error! Option --predicted_labels not found!" << endl;
            return 1;
        }
            // File to save predictions
        string prediction_file = cmd.optionValue("predicted_labels");
            // Predict data
        PredictData(data_file, model_file, prediction_file);
    }
}